package com.sirius.koshelek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableCaching
@EnableJpaAuditing
@SpringBootApplication
@EnableFeignClients
@ConfigurationPropertiesScan
public class KoshelekApplication {

    public static void main(String[] args) {
        SpringApplication.run(KoshelekApplication.class, args);
    }

}
