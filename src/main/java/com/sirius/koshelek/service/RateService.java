package com.sirius.koshelek.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sirius.koshelek.client.RateClient;
import com.sirius.koshelek.config.RateProperties;
import com.sirius.koshelek.model.request.RateRequest;
import com.sirius.koshelek.model.response.RateResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = "rate")
@RequiredArgsConstructor
public class RateService {

    private final RateClient rateClient;
    private final RateProperties rateProperties;

    @Cacheable
    public RateResponse getRates(RateRequest rateRequest) {
        String base = rateRequest.getBase();

        String pairs = getPairs(rateRequest);

        String result = rateClient.findRate(rateProperties.getMethod(), pairs, rateProperties.getKey());

        JsonObject json = new JsonParser().parse(result).getAsJsonObject();

        Map<String, String> data = new Gson().fromJson(json.get("data"), HashMap.class);
        Map<String, String> responseData = new HashMap<>();

        data.forEach((key, value) -> responseData.put(key.replaceFirst(base, ""), value.substring(0, 6)));

        return new RateResponse(responseData);
    }

    private String getPairs(RateRequest rateRequest) {
        final String base = rateRequest.getBase();
        return rateRequest.getCodes().stream().map(x -> x + base).collect(Collectors.joining(","));
    }
}
