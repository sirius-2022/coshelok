package com.sirius.koshelek.service;

import com.sirius.koshelek.model.response.WalletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailService {

    private final UserService userService;
    private final WalletService walletService;
    private final OperationService operationService;

    private final JavaMailSender mailSender;

    public void postAll() {
        List<String> logins = userService.getLogins();
        String emailFrom = "koshelok227@rambler.ru";
        String subject = "PiggyBen";

        for (String login: logins) {

            String hello = String.format("Привет %s, за все время использования приложения ты сделял эти операции !", login);
            StringBuilder message = new StringBuilder(hello);

            List<WalletResponse> wallets = walletService.getAllByUserLogin(login);
            if (wallets.isEmpty())
                continue;

            for (WalletResponse wallet: wallets) {
                int value = operationService.getOperations(login, wallet.getId(), null, null).size();
                String messageWallet = String.format("По кошельку %s : %d !", wallet.getName(), value);
                message.append(messageWallet);
            }

            send(emailFrom, login, subject, message.toString());
        }

    }

    private void send(String emailFrom, String emailTo, String subject, String message) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();

        mailMessage.setFrom(emailFrom);
        mailMessage.setTo(emailTo);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);

        mailSender.send(mailMessage);
    }

}
