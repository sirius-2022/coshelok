package com.sirius.koshelek.service;

import com.sirius.koshelek.converter.CategoryConverter;
import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.db.repository.CategoryRepository;
import com.sirius.koshelek.exceptions.NoSuchCategoryException;
import com.sirius.koshelek.model.OperationType;
import com.sirius.koshelek.model.response.CategoryResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "category")
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    private final CategoryConverter categoryConverter;

    @Cacheable
    public List<CategoryResponse> getAllByType(OperationType type) {
        return categoryRepository.findByType(type).stream()
                .map(categoryConverter::toCategoryResponse)
                .toList();
    }

    @Cacheable
    public Category get(Long id) {
        validate(id);

        return categoryRepository.getReferenceById(id);
    }


    private void validate(long category) {
        if (!categoryRepository.existsById(category)) {
            throw new NoSuchCategoryException(category);
        }
    }

}
