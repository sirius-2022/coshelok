package com.sirius.koshelek.service;

import com.sirius.koshelek.converter.CurrencyConverter;
import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.db.repository.CurrencyRepository;
import com.sirius.koshelek.exceptions.NoSuchCurrencyException;
import com.sirius.koshelek.model.response.CurrencyResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "currency")
@RequiredArgsConstructor
public class CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final CurrencyConverter currencyConverter;

    @Cacheable
    public List<CurrencyResponse> getAll() {
        return currencyConverter.toCurrencyResponse(currencyRepository.findAll());
    }

    @Cacheable
    public Currency get(Long currency) {
        validate(currency);

        return currencyRepository.getReferenceById(currency);
    }

    private void validate(Long currency) {
        if (!currencyRepository.existsById(currency)) {
            throw new NoSuchCurrencyException(currency);
        }
    }
}
