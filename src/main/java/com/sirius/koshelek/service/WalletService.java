package com.sirius.koshelek.service;

import com.sirius.koshelek.converter.WalletConverter;
import com.sirius.koshelek.db.entity.Wallet;
import com.sirius.koshelek.db.repository.WalletRepository;
import com.sirius.koshelek.exceptions.NoSuchWalletException;
import com.sirius.koshelek.model.request.WalletRequest;
import com.sirius.koshelek.model.response.WalletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class WalletService {

    private final WalletRepository walletRepository;
    private final WalletConverter walletConverter;
    private final UserService userService;
    private final CurrencyService currencyService;

    public Wallet get(Long id) {
        return walletRepository.getReferenceById(id);
    }

    public WalletResponse create(WalletRequest walletRequest, String login) {
        Wallet wallet = walletConverter.toWallet(walletRequest);
        wallet.setUserId(userService.getUserIdByLogin(login));
        wallet.setCurrency(currencyService.get(walletRequest.getCurrency()));

        wallet.setIncome(BigDecimal.valueOf(0));
        wallet.setOutcome(BigDecimal.valueOf(0));
        wallet.setIsDeleted(false);

        log.info("Create wallet for login:{}, walletRequest:{}\nwallet:{}", login, walletRequest, wallet);
        return walletConverter.toWalletResponse(walletRepository.save(wallet));
    }

    public void delete(Long id, String login) {
        validateWallet(login, id);
        Wallet wallet = walletRepository.getReferenceById(id);
        wallet.setIsDeleted(true);

        log.info("Delete wallet for login:{}, wallet:{}", login, wallet);
        walletRepository.save(wallet);
    }

    public List<WalletResponse> getAllByUserLogin(String login) {
        return walletConverter.toWalletResponse(
                walletRepository.findAllByUserId(userService.getUserIdByLogin(login))
                        .stream().filter(i -> !i.getIsDeleted()).toList()
        );
    }

    public WalletResponse change(WalletRequest walletRequest, Long id, String login) {
        validateWallet(login, id);

        Wallet wallet = walletRepository.getReferenceById(id);
        wallet.setCurrency(currencyService.get(walletRequest.getCurrency()));
        wallet.setName(walletRequest.getName());
        wallet.setLimit(walletRequest.getLimit());

        log.info("Change wallet for login:{}, wallet:{}\nwalletRequest:{}", login, wallet, walletRequest);
        return walletConverter.toWalletResponse(walletRepository.save(wallet));
    }

    public void save(Wallet wallet) {
        walletRepository.save(wallet);
    }

    public void validateWallet(String login, Long id) {
        if (!walletRepository.existsById(id) || !belongsToUser(id, login)) {
            throw new NoSuchWalletException(id);
        }
    }

    private boolean belongsToUser(Long id, String login) {
        Wallet wallet = walletRepository.getReferenceById(id);
        return !wallet.getIsDeleted() && wallet.getUserId().equals(userService.getUserIdByLogin(login));
    }

}
