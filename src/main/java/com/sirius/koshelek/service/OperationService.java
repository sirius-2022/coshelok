package com.sirius.koshelek.service;

import com.sirius.koshelek.converter.OperationConverter;
import com.sirius.koshelek.db.entity.Operation;
import com.sirius.koshelek.db.entity.Wallet;
import com.sirius.koshelek.db.repository.OperationRepository;
import com.sirius.koshelek.exceptions.NoSuchWalletException;
import com.sirius.koshelek.model.OperationType;
import com.sirius.koshelek.model.request.OperationRequest;
import com.sirius.koshelek.model.response.OperationResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class OperationService {

    private final OperationConverter operationConverter;
    private final OperationRepository operationRepository;
    private final CategoryService categoryService;
    private final CurrencyService currencyService;
    private final WalletService walletService;

    public List<OperationResponse> getOperations(String login, Long walledId, Integer page, Integer size) {
        walletService.validateWallet(login, walledId);

        PageRequest pageRequest = getPageRequest(page, size);

        return pageRequest == null
                ? operationConverter.toOperationResponse(operationRepository.findAllByWalletId(walledId))
                : operationConverter.toOperationResponse(operationRepository.findAllByWalletId(walledId, pageRequest));
    }

    public OperationResponse getOperation(String login, Long walletId, Long id) {
        validateOperation(login, walletId, id);

        return operationConverter.toOperationResponse(operationRepository.getReferenceById(id));
    }

    public OperationResponse create(String login, Long walletId, OperationRequest operationRequest) {
        Operation operation = operationConverter.toOperation(
                operationRequest,
                categoryService.get(operationRequest.getCategory()),
                currencyService.get(operationRequest.getCurrency())
        );
        operation.setWalletId(walletId);
        defaultDate(operation);

        return create(login, walletId, operation);
    }

    public OperationResponse create(String login, Long walletId, Operation operation) {
        walletService.validateWallet(login, walletId);

        Wallet wallet = walletService.get(operation.getWalletId());

        log.info("Create operation for login:{}, wallet:{}\noperation:{}", login, wallet, operation);
        addOperationForWallet(wallet, operation.getType(), operation.getValue());

        log.info("Updated wallet:{}", wallet);
        walletService.save(wallet);
        return operationConverter.toOperationResponse(operationRepository.save(operation));
    }

    public OperationResponse change(String login, Long walletId, Long id, OperationRequest operationRequest) {
        validateOperation(login, walletId, id);

        Operation operation = operationRepository.getReferenceById(id);
        Operation newOperation = operationConverter.toOperation(
                operationRequest,
                categoryService.get(operationRequest.getCategory()),
                currencyService.get(operationRequest.getCurrency())
        );
        Wallet wallet = walletService.get(operation.getWalletId());
        defaultDate(newOperation);

        log.info("Change operation for login:{}, wallet:{}\noperation:{}\nnewOperation:{}", login, wallet, operation, newOperation);
        subtractOperationForWallet(wallet, operation.getType(), operation.getValue());
        update(operation, newOperation);
        addOperationForWallet(wallet, operation.getType(), operation.getValue());

        log.info("Updated wallet:{}", wallet);
        walletService.save(wallet);
        return operationConverter.toOperationResponse(operationRepository.save(operation));
    }

    public void delete(String login, Long walletId, Long id) {
        validateOperation(login, walletId, id);

        Operation operation = operationRepository.getReferenceById(id);
        Wallet wallet = walletService.get(operation.getWalletId());

        log.info("Delete operation for login:{}, wallet:{}\noperation:{}", login, wallet, operation);
        subtractOperationForWallet(wallet, operation.getType(), operation.getValue());

        log.info("Updated wallet:{}", wallet);
        walletService.save(wallet);
        operationRepository.deleteById(id);
    }

    private void validateOperation(String login, Long walletId, Long id) {
        walletService.validateWallet(login, walletId);

        if (!operationRepository.existsById(id) || !belongsToWallet(walletId, id)) {
            throw new NoSuchWalletException(id);
        }
    }

    private boolean belongsToWallet(Long walletId, Long id) {
        return operationRepository.getReferenceById(id).getWalletId().equals(walletId);
    }

    private void subtractOperationForWallet(Wallet wallet, OperationType type, BigDecimal value) {
        if (type.equals(OperationType.INCOME)) {
            wallet.setIncome(wallet.getIncome().subtract(value));
        } else {
            wallet.setOutcome(wallet.getOutcome().subtract(value));
        }
    }

    private void addOperationForWallet(Wallet wallet, OperationType type, BigDecimal value) {
        if (type.equals(OperationType.INCOME)) {
            wallet.setIncome(wallet.getIncome().add(value));
        } else {
            wallet.setOutcome(wallet.getOutcome().add(value));
        }
    }

    private void update(Operation operation, Operation newOperation) {
        operation.setType(newOperation.getType());
        operation.setValue(newOperation.getValue());
        operation.setDate(newOperation.getDate());
        operation.setCategory(newOperation.getCategory());
    }

    private void defaultDate(Operation operation) {
        if (operation.getDate() == null) {
            operation.setDate(LocalDateTime.now());
        }
    }

    private PageRequest getPageRequest(Integer page, Integer size) {
        return page == null || size == null ? null : PageRequest.of(page, size);
    }
}
