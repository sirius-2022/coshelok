package com.sirius.koshelek.service;

import com.sirius.koshelek.model.response.AccountResponse;
import com.sirius.koshelek.model.response.CurrencyResponse;
import com.sirius.koshelek.model.response.WalletResponse;
import com.sirius.koshelek.model.response.WalletsResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

    private final WalletService walletService;

    public AccountResponse getAccount(String login) {
        List<WalletResponse> wallets = walletService.getAllByUserLogin(login);
        log.info("getAccount for login:{}\nwallets:{}", login, wallets);
        return new AccountResponse(login, currenciesToWallets(wallets));
    }

    private Map<String, WalletsResponse> currenciesToWallets(List<WalletResponse> walletsResponses) {
        return walletsResponses.stream()
                .collect(groupingBy(i -> i.getCurrency().getCode(),
                        collectingAndThen(toList(), wallets -> {
                            CurrencyResponse currency = wallets.get(0).getCurrency();
                            BigDecimal balance = sum(wallets, WalletResponse::getBalance);
                            BigDecimal income = sum(wallets, WalletResponse::getIncome);
                            BigDecimal outcome = sum(wallets, WalletResponse::getOutcome);

                            return new WalletsResponse(wallets, currency, balance, income, outcome);
                        })));
    }

    private BigDecimal sum(List<WalletResponse> wallets, Function<WalletResponse, BigDecimal> getField) {
        return wallets.stream().map(getField).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
