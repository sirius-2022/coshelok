package com.sirius.koshelek.service;


import com.sirius.koshelek.db.entity.User;
import com.sirius.koshelek.db.repository.UserRepository;
import com.sirius.koshelek.exceptions.NoSuchUserException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = "user")
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    @Cacheable
    public User getUser(String login) {
        if (!userRepository.existsUserByLogin(login)) {
            throw new NoSuchUserException(login);
        }

        return userRepository.findByLogin(login);
    }

    public Long getUserIdByLogin(String login) {
        return getUser(login).getId();
    }

    public List<String> getLogins() {
        return userRepository.findAll().stream().map(User::getLogin).collect(Collectors.toList());
    }
}
