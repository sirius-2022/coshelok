package com.sirius.koshelek.service;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.sirius.koshelek.config.CategoriesProperties;
import com.sirius.koshelek.config.CurrencyProperties;
import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.db.entity.Operation;
import com.sirius.koshelek.model.OperationType;
import com.sirius.koshelek.model.response.OperationResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;

@Service
@RequiredArgsConstructor
public class FileService {
    private final Long OTHER_CATEGORY_INCOME = 13L;
    private final Long OTHER_CATEGORY_OUTCOME = 14L;

    // start as "17.07.22 16:..."
    private final Predicate<String> operation =
            Pattern.compile("^\\w*\\d\\d\\.\\d\\d\\.\\d\\d  \\d\\d:").asPredicate();

    private final OperationService operationService;

    private final CategoryService categoryService;

    private final CurrencyService currencyService;

    private final CategoriesProperties categoriesProperties;

    private final CurrencyProperties currencyProperties;

    @SneakyThrows
    public List<OperationResponse> createOperations(byte[] file, String login, Long walletId) {
        PdfReader pdfReader = new PdfReader(file);
        int pagesCount = pdfReader.getNumberOfPages();

        List<Operation> operations = new ArrayList<>();
        for (int i = 1; i <= pagesCount; i++) {
            String pageContent = PdfTextExtractor.getTextFromPage(pdfReader, i);

            operations.addAll(Arrays.stream(pageContent.split("\n"))
                    .filter(operation)
                    .map(s -> stringToOperation(walletId, s))
                    .toList());
        }
        pdfReader.close();

        return operations.stream()
                .map(i -> operationService.create(login, walletId, i))
                .toList();

    }

    private Operation stringToOperation(Long walletId, String s) {
        List<String> parts = Arrays.stream(s.split(" "))
                .filter(Predicate.not(String::isBlank))
                .toList();

        return parseOperation(walletId, parts);
    }

    private Operation parseOperation(Long walletId, List<String> parts) {
        Operation operation = new Operation();

        operation.setWalletId(walletId);

        operation.setDate(parseDate(parts.get(0), parts.get(1)));

        operation.setType(parts.get(3).startsWith("+") ? OperationType.INCOME : OperationType.OUTCOME);

        StringBuilder sum = new StringBuilder(parts.get(3));
        int index = 4;
        while (Character.isDigit(parts.get(index).charAt(0))) {
            sum.append(parts.get(index));
            index++;
        }
        operation.setValue(BigDecimal.valueOf(Double.parseDouble(sum.toString())));

        operation.setCurrency(currencyService.get(currencyProperties.getCurrencies().get(parts.get(index))));

        // skip second sum
        index += index - 2;

        operation.setCategory(parseCategory(index, parts, operation.getType()));

        return operation;
    }

    private Category parseCategory(int index, List<String> parts, OperationType type) {
        for (int i = index; i < parts.size(); i++) {
            String category = categoriesProperties.getCategories().get(parts.get(i));
            if (category != null) {
                return categoryService.get(Long.parseLong(category));
            }
        }

        return categoryService.get(type == OperationType.INCOME ? OTHER_CATEGORY_INCOME : OTHER_CATEGORY_OUTCOME);
    }


    private LocalDateTime parseDate(String date, String time) {
        List<String> dateList = Arrays.stream(date.split("\\.")).toList();
        String correctDate = "20" + dateList.get(2) + "-" + dateList.get(1) + "-" + dateList.get(0);
        return LocalDateTime.parse(correctDate + "T" + time + ":00");
    }

}
