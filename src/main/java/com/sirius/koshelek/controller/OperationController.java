package com.sirius.koshelek.controller;

import com.sirius.koshelek.model.request.OperationRequest;

import com.sirius.koshelek.model.response.OperationResponse;
import com.sirius.koshelek.service.OperationService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;

import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController()
@RequestMapping("/operations")
@RequiredArgsConstructor
public class OperationController {

    private final OperationService operationService;

    @Operation(summary = "Get operations")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OperationResponse> getOperations(@RequestHeader("login") String login,
                                                 @RequestHeader("walletId") Long walletId,
                                                 @RequestParam(required = false) Integer page,
                                                 @RequestParam(required = false) Integer size) {

        return operationService.getOperations(login, walletId, page, size);
    }

    @Operation(summary = "Get operation")
    @GetMapping(value = "{operationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public OperationResponse getOperation(@RequestHeader("login") String login,
                                          @RequestHeader("walletId") Long walletId,
                                          @PathVariable Long operationId) {
        return operationService.getOperation(login, walletId, operationId);
    }

    @Operation(summary = "Create operation")
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public OperationResponse createOperation(@RequestHeader("login") String login,
                                             @RequestHeader("walletId") Long walletId,
                                             @Valid @RequestBody OperationRequest operation) {
        return operationService.create(login, walletId, operation);
    }

    @Operation(summary = "Change operation")
    @PutMapping(value = "/{operationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public OperationResponse changeOperation(@RequestHeader("login") String login,
                                             @RequestHeader("walletId") Long walletId,
                                             @PathVariable Long operationId,
                                             @Valid @RequestBody OperationRequest operation) {
        return operationService.change(login, walletId, operationId, operation);
    }

    @Operation(summary = "Create operation")
    @DeleteMapping(value = "{operationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteOperation(@RequestHeader("login") String login,
                                @RequestHeader("walletId") Long walletId,
                                @PathVariable Long operationId) {
        operationService.delete(login, walletId, operationId);
    }

}
