package com.sirius.koshelek.controller;

import com.sirius.koshelek.model.response.AccountResponse;
import com.sirius.koshelek.service.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/accounts")
@RestController()
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @Operation(summary = "Get account by userId")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public AccountResponse getAccount(@RequestHeader("login") String login) {
        return accountService.getAccount(login);
    }

}
