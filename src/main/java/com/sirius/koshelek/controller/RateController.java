package com.sirius.koshelek.controller;

import com.sirius.koshelek.model.request.RateRequest;
import com.sirius.koshelek.model.response.RateResponse;
import com.sirius.koshelek.service.RateService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/rates")
@RestController()
@RequiredArgsConstructor
public class RateController {
    private final RateService rateService;

    @Operation(summary = "Get rates")
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public RateResponse getRates(@Valid @RequestBody RateRequest rateRequest) {
        return rateService.getRates(rateRequest);
    }

}
