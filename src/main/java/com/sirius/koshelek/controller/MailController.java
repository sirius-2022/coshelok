package com.sirius.koshelek.controller;

import com.sirius.koshelek.service.MailService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/mail")
@RestController()
@RequiredArgsConstructor
public class MailController {

    private final MailService mailService;

    @Operation(summary = "Post email")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public void postEmail() {
        mailService.postAll();
    }

}
