package com.sirius.koshelek.controller;

import com.sirius.koshelek.model.request.WalletRequest;
import com.sirius.koshelek.model.response.WalletResponse;
import com.sirius.koshelek.service.WalletService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController()
@RequestMapping("/wallets")
@RequiredArgsConstructor
public class WalletController {

    private final WalletService walletService;

    @Operation(summary = "Get wallets")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WalletResponse> getWallets(@RequestHeader("login") String login) {
        return walletService.getAllByUserLogin(login);
    }

    @Operation(summary = "Create wallet")
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public WalletResponse createWallet(@Valid @RequestBody WalletRequest walletRequest, @RequestHeader("login") String login) {
        return walletService.create(walletRequest, login);
    }

    @Operation(summary = "Change wallet")
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public WalletResponse changeWallet(
            @Valid @RequestBody WalletRequest walletRequest, @PathVariable Long id, @RequestHeader("login") String login
    ) {
        return walletService.change(walletRequest, id, login);
    }

    @Operation(summary = "Delete wallet")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteWallet(@PathVariable Long id, @RequestHeader("login") String login) {
        walletService.delete(id, login);
    }
}
