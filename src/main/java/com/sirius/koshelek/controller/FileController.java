package com.sirius.koshelek.controller;

import com.sirius.koshelek.model.response.OperationResponse;
import com.sirius.koshelek.service.FileService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/file")
@RestController()
@RequiredArgsConstructor
public class FileController {

    private final FileService fileService;

    @Operation(summary = "Create operations by file")
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OperationResponse> createOperations(@RequestBody Byte[] file,
                                                 @RequestHeader("login") String login,
                                                 @RequestHeader("walletId") Long walletId) {

        return fileService.createOperations(ArrayUtils.toPrimitive(file), login, walletId);
    }

}
