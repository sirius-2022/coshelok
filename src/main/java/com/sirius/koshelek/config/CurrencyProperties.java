package com.sirius.koshelek.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import java.util.Map;

@Getter
@Setter
@Validated
@Configuration
public class CurrencyProperties {
    private Map<String, Long> currencies = Map.of(
            "₽", 0L,
            "$", 1L,
            "€", 2L
    );
}
