package com.sirius.koshelek.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@Schema
public class WalletResponse {

    @Schema(required = true)
    private final long id;

    @Schema(required = true)
    @NotBlank
    private final String name;

    @Schema(required = true)
    private final BigDecimal limit;

    @Schema(required = true)
    private final BigDecimal balance;

    @Schema(required = true)
    private final BigDecimal income;

    @Schema(required = true)
    private final BigDecimal outcome;

    @Schema(required = true)
    private final CurrencyResponse currency;

    @Schema(required = true)
    private final Boolean overLimit;

}
