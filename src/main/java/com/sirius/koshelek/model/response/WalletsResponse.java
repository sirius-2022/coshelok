package com.sirius.koshelek.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Schema
@Data
public class WalletsResponse {

    @Schema(required = true)
    private final List<WalletResponse> wallets;

    @Schema(required = true)
    private final CurrencyResponse currency;

    @Schema(required = true)
    private final BigDecimal balance;

    @Schema(required = true)
    private final BigDecimal income;

    @Schema(required = true)
    private final BigDecimal outcome;

}
