package com.sirius.koshelek.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Map;

@Data
@Schema
public class RateResponse {

    @Schema(required = true)
    private final Map<String, String> rates;

}
