package com.sirius.koshelek.model.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Map;

@Schema
@Data
public class AccountResponse {

    @Schema(required = true)
    private final String login;

    @Schema(required = true)
    private final Map<String, WalletsResponse> wallets;

}