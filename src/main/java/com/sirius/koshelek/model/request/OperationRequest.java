package com.sirius.koshelek.model.request;

import com.sirius.koshelek.model.OperationType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Schema
public class OperationRequest {

    @Schema(required = true)
    @NotNull(message = "Type must not be null")
    private final OperationType type;

    @Schema(required = true)
    @NotNull(message = "Category must not be null")
    private final Long category;

    @Schema(required = true)
    @NotNull(message = "Currency must not be null")
    private final Long currency;

    @Schema(required = true)
    @NotNull(message = "Value must not be null")
    @Min(value = 0, message = "Must be greater than zero")
    private final BigDecimal value;

    @Schema(required = true)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private final LocalDateTime date;
}
