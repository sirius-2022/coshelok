package com.sirius.koshelek.model.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Schema
public class WalletRequest {

    @Schema(required = true)
    @NotBlank(message = "Name must not be null")
    private final String name;

    @Schema(required = true)
    @NotNull(message = "Currency must not be null")
    private final Long currency;

    @Schema(required = true)
    @Min(value = 0, message = "Must be greater than zero")
    private final BigDecimal limit;

}
