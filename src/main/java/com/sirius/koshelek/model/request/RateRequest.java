package com.sirius.koshelek.model.request;

import com.sun.istack.NotNull;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Schema
public class RateRequest {

    @Schema(required = true)
    @NotNull
    private final List<String> codes;

    @Schema(required = true)
    @NotBlank
    private final String base;
}
