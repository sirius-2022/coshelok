package com.sirius.koshelek.exceptions;

public class NoSuchWalletException extends NoSuchElementException {

    public NoSuchWalletException(Long id) {
        super("No such wallet: " + id);
    }
}
