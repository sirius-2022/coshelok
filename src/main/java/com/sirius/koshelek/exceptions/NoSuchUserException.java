package com.sirius.koshelek.exceptions;

public class NoSuchUserException extends NoSuchElementException {

    public NoSuchUserException(String login) {
        super("No such user with login: " + login);
    }
}
