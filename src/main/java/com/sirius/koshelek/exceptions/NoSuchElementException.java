package com.sirius.koshelek.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public abstract class NoSuchElementException extends RuntimeException {

    public NoSuchElementException(String message) {
        super(message);
    }

}
