package com.sirius.koshelek.exceptions;

public class NoSuchCategoryException extends NoSuchElementException {

    public NoSuchCategoryException(Long id) {
        super("No such category: " + id);
    }

}
