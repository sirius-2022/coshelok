package com.sirius.koshelek.exceptions;

public class NoSuchCurrencyException extends NoSuchElementException {

    public NoSuchCurrencyException(Long currency) {
        super("No such currency: " + currency);
    }

}
