package com.sirius.koshelek.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "client-rate", url = "${rate.url}")
public interface RateClient {

    @GetMapping("/")
    String findRate(@RequestParam("get") String method,
                    @RequestParam("pairs") String pairs,
                    @RequestParam("key") String key);

}
