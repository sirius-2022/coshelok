package com.sirius.koshelek.converter;

import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.model.request.CategoryRequest;
import com.sirius.koshelek.model.response.CategoryResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CategoryConverter {

    @Mapping(target = "id", ignore = true)
    Category toCategory(CategoryRequest categoryRequest);

    CategoryResponse toCategoryResponse(Category category);
}
