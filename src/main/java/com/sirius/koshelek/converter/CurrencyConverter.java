package com.sirius.koshelek.converter;

import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.model.response.CurrencyResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CurrencyConverter {

    CurrencyResponse toCurrencyResponse(Currency currency);

    List<CurrencyResponse> toCurrencyResponse(List<Currency> currencies);
}
