package com.sirius.koshelek.converter;

import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.db.entity.Operation;
import com.sirius.koshelek.model.request.OperationRequest;
import com.sirius.koshelek.model.response.OperationResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CurrencyConverter.class, CategoryConverter.class})
public interface OperationConverter {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "walletId", ignore = true)
    @Mapping(target = "type", source = "operation.type")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "currency", source = "currency")
    Operation toOperation(OperationRequest operation, Category category, Currency currency);

    OperationResponse toOperationResponse(Operation operation);

    List<OperationResponse> toOperationResponse(List<Operation> operation);
}
