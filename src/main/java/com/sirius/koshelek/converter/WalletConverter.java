package com.sirius.koshelek.converter;

import com.sirius.koshelek.db.entity.Wallet;
import com.sirius.koshelek.model.request.WalletRequest;
import com.sirius.koshelek.model.response.WalletResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring", uses = CurrencyConverter.class)
public interface WalletConverter {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "income", ignore = true)
    @Mapping(target = "outcome", ignore = true)
    @Mapping(target = "currency.id", source = "walletRequest.currency")
    @Mapping(target = "isDeleted", ignore = true)
    Wallet toWallet(WalletRequest walletRequest);


    @Mapping(target = "overLimit", source = "wallet", qualifiedByName = "toLimit")
    @Mapping(target = "balance", expression = "java(wallet.getIncome().subtract(wallet.getOutcome()))")
    WalletResponse toWalletResponse(Wallet wallet);

    List<WalletResponse> toWalletResponse(List<Wallet> wallets);

    @Named("toLimit")
    default boolean toLimit(Wallet wallet) {
        return wallet.getLimit() != null && wallet.getOutcome().compareTo(wallet.getLimit()) > 0;
    }
}
