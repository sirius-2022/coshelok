package com.sirius.koshelek.converter;

import com.sirius.koshelek.db.entity.User;
import com.sirius.koshelek.model.request.UserRequest;
import com.sirius.koshelek.model.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserConverter {

    @Mapping(target = "id", ignore = true)
    User toUser(UserRequest userRequest);

    UserResponse toUserResponse(User user);
}
