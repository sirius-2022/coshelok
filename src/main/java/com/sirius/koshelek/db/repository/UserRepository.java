package com.sirius.koshelek.db.repository;

import com.sirius.koshelek.db.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(String login);

    boolean existsUserByLogin(String login);
}
