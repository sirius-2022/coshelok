package com.sirius.koshelek.db.repository;

import com.sirius.koshelek.db.entity.Operation;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
    List<Operation> findAllByWalletId(Long walletId, Pageable pageable);

    List<Operation> findAllByWalletId(Long walletId);
}
