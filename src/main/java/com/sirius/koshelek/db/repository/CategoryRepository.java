package com.sirius.koshelek.db.repository;

import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.model.OperationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    List<Category> findByType(OperationType type);

}
