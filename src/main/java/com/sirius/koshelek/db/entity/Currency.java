package com.sirius.koshelek.db.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table
@Entity(name = "currencies")
@SequenceGenerator(allocationSize = 1, name = "currency_seq", sequenceName = "currency_seq")
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "currency_seq")
    private Long id;

    private String name;

    private String code;
}
