package com.sirius.koshelek.db.entity;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Table
@Entity(name = "wallets")
@SequenceGenerator(allocationSize = 1, name = "wallet_seq", sequenceName = "wallet_seq")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "wallet_seq")
    private Long id;

    private Long userId;

    private String name;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;

    @Column(name = "bound")
    private BigDecimal limit;

    private BigDecimal income;

    private BigDecimal outcome;

    @Column(name = "is_deleted")
    private Boolean isDeleted;
}
