package com.sirius.koshelek.service;

import com.sirius.koshelek.model.request.RateRequest;
import com.sirius.koshelek.model.response.RateResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class RateServiceTest extends AbstractServiceTest {

    @Autowired
    RateService rateService;

    private final String JSONResult = "{\"status\":200,\"message\":\"rates\",\"data\":{\"USDRUB\":\"64.1824\",\"EURRUB\":\"69.244\"}}";

    @BeforeEach
    void init() {
        when(rateClient.findRate(any(),any(),any()))
                .thenReturn(JSONResult);

    }

    @Test
    void getRates() {
        RateResponse rateResponse = rateService.getRates(new RateRequest(List.of("USD", "EUR"), "RUB"));

        Map<String, String> expected = new HashMap<>();
        expected.put("USD", "64.182");
        expected.put("EUR", "69.244");

        assertThat(new RateResponse(expected)).isEqualTo(rateResponse);
    }

}
