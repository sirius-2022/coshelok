package com.sirius.koshelek.service;

import com.sirius.koshelek.converter.CategoryConverter;
import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.model.OperationType;
import com.sirius.koshelek.model.response.CategoryResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;

import static com.sirius.koshelek.data.CategoryData.INCOME_CATEGORIES;
import static com.sirius.koshelek.data.CategoryData.OUTCOME_CATEGORIES;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

public class CategoryServiceTest extends AbstractServiceTest {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryConverter categoryConverter;

    @Test
    public void testIncomeCategories() {
        testCategories(OperationType.INCOME, INCOME_CATEGORIES);
    }

    @Test
    public void testOutcomeCategories() {
        testCategories(OperationType.OUTCOME, OUTCOME_CATEGORIES);
    }

    @Test
    public void testEmptyCategories() {
        testCategories(OperationType.INCOME, Collections.emptyList());
        testCategories(OperationType.OUTCOME, Collections.emptyList());
    }

    public void testCategories(OperationType type, List<Category> expectedCategories) {
        when(categoryRepository.findByType(type)).thenReturn(expectedCategories);

        List<CategoryResponse> real = categoryService.getAllByType(type);
        List<CategoryResponse> expected = expectedCategories.stream()
                .map(categoryConverter::toCategoryResponse).toList();

        assertTrue(real.size() == expected.size() && real.containsAll(expected) && expected.containsAll(real));
    }
}
