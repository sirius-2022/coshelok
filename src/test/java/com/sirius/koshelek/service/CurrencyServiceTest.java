package com.sirius.koshelek.service;

import com.sirius.koshelek.builder.entity.CurrencyBuilder;
import com.sirius.koshelek.builder.response.CurrencyResponseBuilder;
import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.model.response.CurrencyResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class CurrencyServiceTest extends AbstractServiceTest {

    @Autowired
    CurrencyService currencyService;

    @BeforeEach
    void init() {
        when(currencyRepository.findAll())
                .thenReturn(List.of(CurrencyBuilder.aCurrency().build()));

        when(currencyRepository.getReferenceById(any()))
                .thenReturn(CurrencyBuilder.aCurrency().build());

        when(currencyRepository.existsById(any()))
                .thenReturn(true);
    }

    @Test
    void getAll() {
        List<CurrencyResponse> currencyResponses = currencyService.getAll();

        assertThat(List.of(CurrencyResponseBuilder.aCurrencyResponse().build())).isEqualTo(currencyResponses);
    }

    @Test
    void get() {
        Currency currencyResponse = currencyService.get(0L);

        assertThat(CurrencyBuilder.aCurrency().build()).isEqualTo(currencyResponse);
    }
}
