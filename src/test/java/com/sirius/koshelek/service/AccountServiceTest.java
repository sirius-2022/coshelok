package com.sirius.koshelek.service;

import com.sirius.koshelek.converter.WalletConverter;
import com.sirius.koshelek.db.entity.Wallet;
import com.sirius.koshelek.model.response.AccountResponse;
import com.sirius.koshelek.model.response.CurrencyResponse;
import com.sirius.koshelek.model.response.WalletResponse;
import com.sirius.koshelek.model.response.WalletsResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.sirius.koshelek.data.CurrencyData.*;
import static com.sirius.koshelek.data.WalletsData.createWallet;
import static com.sirius.koshelek.data.WalletsData.money;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class AccountServiceTest extends AbstractServiceTest {

    @Autowired
    private AccountService accountService;

    @MockBean
    private WalletService walletService;

    @Autowired
    private WalletConverter walletConverter;

    @Test
    public void testEmpty() {
        when(walletService.getAllByUserLogin("user1")).thenReturn(Collections.emptyList());

        AccountResponse real = accountService.getAccount("user1");
        AccountResponse expected = new AccountResponse("user1", Collections.emptyMap());

        assertEquals(real.getLogin(), expected.getLogin());
        assertEquals(expected.getWallets(), real.getWallets());
    }

    @Test
    public void getAccounts() {
        List<Wallet> wallets = List.of(
                createWallet(RUB, 500, 700, 200),
                createWallet(RUB, 1000, 1000, 0),
                createWallet(USD, 0, 0, 0),
                createWallet(USD, 500, 2000, 1500),
                createWallet(EUR, 1_000_000_000, 1_000_000_000, 0)
        );

        List<WalletResponse> walletResponses = walletConverter.toWalletResponse(wallets);
        when(walletService.getAllByUserLogin("user1")).thenReturn(walletResponses);

        AccountResponse expected = new AccountResponse("user1", Map.of(
                "RUB", new WalletsResponse(getWalletsByCurrency(walletResponses, RUB_RESPONSE),
                        RUB_RESPONSE,
                        money(1500),
                        money(1700),
                        money(200)),
                "USD", new WalletsResponse(getWalletsByCurrency(walletResponses, USD_RESPONSE),
                        USD_RESPONSE,
                        money(500),
                        money(2000),
                        money(1500)),
                "EUR", new WalletsResponse(getWalletsByCurrency(walletResponses, EUR_RESPONSE),
                        EUR_RESPONSE,
                        money(1_000_000_000),
                        money(1_000_000_000),
                        money(0))
        ));

        AccountResponse real = accountService.getAccount("user1");

        assertEquals(expected.getLogin(), real.getLogin());
        assertEquals(expected.getWallets(), real.getWallets());
    }

    private List<WalletResponse> getWalletsByCurrency(List<WalletResponse> wallets, CurrencyResponse currency) {
        return wallets.stream().filter(i -> i.getCurrency().equals(currency)).toList();
    }

}
