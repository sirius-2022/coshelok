package com.sirius.koshelek.service;

import com.sirius.koshelek.builder.entity.*;
import com.sirius.koshelek.builder.request.OperationRequestBuilder;
import com.sirius.koshelek.builder.response.OperationResponseBuilder;
import com.sirius.koshelek.db.entity.Operation;
import com.sirius.koshelek.db.entity.Wallet;
import com.sirius.koshelek.model.response.OperationResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

public class OperationServiceTest extends AbstractServiceTest {
    private final String LOGIN = "login";
    private final Long WALLET_ID = 0L;
    private final Long OPERATION_ID = 0L;
    @MockBean
    protected WalletService walletService;
    @MockBean
    protected CurrencyService currencyService;
    @MockBean
    protected CategoryService categoryService;
    @Autowired
    private OperationService operationService;

    @BeforeEach
    void init() {
        when(operationConverter.toOperation(any(), any(), any()))
                .thenReturn(OperationBuilder.aOperation().build());

        when(operationConverter.toOperationResponse(anyList()))
                .thenReturn(List.of(OperationResponseBuilder.aOperationResponse().build()));

        when(operationConverter.toOperationResponse(any(Operation.class)))
                .thenReturn(OperationResponseBuilder.aOperationResponse().build());

        when(userService.getUser(anyString()))
                .thenReturn(UserBuilder.aUser().build());

        when(operationRepository.findAllByWalletId(any()))
                .thenReturn(List.of(OperationBuilder.aOperation().build()));

        when(operationRepository.save(any()))
                .thenReturn(OperationBuilder.aOperation().build());

        when(operationRepository.getReferenceById(anyLong()))
                .thenReturn(OperationBuilder.aOperation().build());

        when(walletService.get(anyLong()))
                .thenReturn(WalletBuilder.aWallet().build());

        when(operationRepository.existsById(any())).thenReturn(true);

        when(currencyService.get(anyLong()))
                .thenReturn(CurrencyBuilder.aCurrency().build());

        when(categoryService.get(anyLong()))
                .thenReturn(CategoryBuilder.aCategory().build());

    }

    @Test
    protected void create() {
        Wallet wallet = WalletBuilder.aWallet().build();
        BigDecimal value = new BigDecimal("200");
        when(walletService.get(anyLong()))
                .thenReturn(wallet);
        when(operationConverter.toOperation(any(), any(), any()))
                .thenReturn(OperationBuilder.aOperation().withValue(value).build());

        OperationResponse operationResponse = operationService.create(
                LOGIN,
                WALLET_ID,
                OperationRequestBuilder.aOperationRequest().withValue(value).build()
        );

        Wallet expected = WalletBuilder.aWallet().build();
        expected.setIncome(expected.getIncome().add(value));

        assertThat(expected).isEqualTo(wallet);
        assertThat(OperationResponseBuilder.aOperationResponse().build()).isEqualTo(operationResponse);
    }

    @Test
    protected void change() {

        OperationResponse operationResponse = operationService.change(
                LOGIN,
                WALLET_ID,
                OPERATION_ID,
                OperationRequestBuilder.aOperationRequest().build()
        );

        assertThat(OperationResponseBuilder.aOperationResponse().build()).isEqualTo(operationResponse);
    }

    @Test
    protected void delete() {
        assertDoesNotThrow(() -> operationService.delete(LOGIN, WALLET_ID, OPERATION_ID));
    }

    @Test
    protected void getOperations() {
        List<OperationResponse> operationResponses = operationService.getOperations(anyString(), any(), 1, 1);

        assertThat(List.of(OperationResponseBuilder.aOperationResponse().build())).isEqualTo(operationResponses);
    }

    @Test
    protected void getOperation() {
        OperationResponse operationResponse = operationService.getOperation(LOGIN, WALLET_ID, OPERATION_ID);

        assertThat(OperationResponseBuilder.aOperationResponse().build()).isEqualTo(operationResponse);
    }
}
