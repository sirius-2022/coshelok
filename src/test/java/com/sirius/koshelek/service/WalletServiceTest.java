package com.sirius.koshelek.service;

import com.sirius.koshelek.builder.entity.CurrencyBuilder;
import com.sirius.koshelek.builder.entity.UserBuilder;
import com.sirius.koshelek.builder.entity.WalletBuilder;
import com.sirius.koshelek.builder.request.WalletRequestBuilder;
import com.sirius.koshelek.builder.response.WalletResponseBuilder;
import com.sirius.koshelek.converter.WalletConverter;
import com.sirius.koshelek.db.entity.Wallet;
import com.sirius.koshelek.model.response.WalletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

public class WalletServiceTest extends AbstractServiceTest {

    @MockBean
    protected WalletConverter walletConverter;

    @MockBean
    protected CurrencyService currencyService;

    @MockBean
    protected CategoryService categoryService;
    @Autowired
    private WalletService walletService;

    @BeforeEach
    void init() {
        when(walletConverter.toWallet(any()))
                .thenReturn(WalletBuilder.aWallet().build());

        when(walletConverter.toWalletResponse(anyList()))
                .thenReturn(List.of(WalletResponseBuilder.aWalletResponse().build()));

        when(walletConverter.toWalletResponse(any(Wallet.class)))
                .thenReturn(WalletResponseBuilder.aWalletResponse().build());

        when(userService.getUser(anyString()))
                .thenReturn(UserBuilder.aUser().build());

        when(walletRepository.findAllByUserId(any()))
                .thenReturn(List.of(WalletBuilder.aWallet().withIsDeleted(false).build()));

        when(walletRepository.save(any()))
                .thenReturn(WalletBuilder.aWallet().build());

        when(walletRepository.existsById(anyLong()))
                .thenReturn(true);


        when(walletRepository.getReferenceById(anyLong()))
                .thenReturn(WalletBuilder.aWallet().withIsDeleted(false).build());

        when(currencyService.get(anyLong()))
                .thenReturn(CurrencyBuilder.aCurrency().build());
    }

    @Test
    protected void create() {
        WalletResponse walletResponse = walletService.create(WalletRequestBuilder.aWalletRequest().build(), anyString());

        assertThat(WalletResponseBuilder.aWalletResponse().build()).isEqualTo(walletResponse);
    }

    @Test
    protected void change() {
        WalletResponse walletResponse = walletService.change(WalletRequestBuilder.aWalletRequest().build(), 0L, anyString());

        assertThat(WalletResponseBuilder.aWalletResponse().build()).isEqualTo(walletResponse);
    }

    @Test
    protected void delete() {
        assertDoesNotThrow(() -> walletService.delete(0L, anyString()));
    }

    @Test
    protected void getByUserLogin() {
        List<WalletResponse> walletResponse = walletService.getAllByUserLogin(anyString());

        assertThat(List.of(WalletResponseBuilder.aWalletResponse().build())).isEqualTo(walletResponse);
    }
}
