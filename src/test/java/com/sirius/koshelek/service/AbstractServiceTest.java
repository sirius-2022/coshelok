package com.sirius.koshelek.service;

import com.sirius.koshelek.client.RateClient;
import com.sirius.koshelek.converter.OperationConverter;
import com.sirius.koshelek.db.repository.CategoryRepository;
import com.sirius.koshelek.db.repository.CurrencyRepository;
import com.sirius.koshelek.db.repository.OperationRepository;
import com.sirius.koshelek.db.repository.WalletRepository;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public abstract class AbstractServiceTest {

    @MockBean
    protected UserService userService;

    @MockBean
    protected WalletRepository walletRepository;

    @MockBean
    protected CategoryRepository categoryRepository;

    @MockBean
    protected OperationConverter operationConverter;

    @MockBean
    protected CurrencyRepository currencyRepository;

    @MockBean
    protected OperationRepository operationRepository;

    @MockBean
    protected RateClient rateClient;

}
