package com.sirius.koshelek.converter;

import com.sirius.koshelek.builder.entity.CurrencyBuilder;
import com.sirius.koshelek.builder.entity.WalletBuilder;
import com.sirius.koshelek.builder.request.WalletRequestBuilder;
import com.sirius.koshelek.builder.response.WalletResponseBuilder;
import com.sirius.koshelek.db.entity.Wallet;
import com.sirius.koshelek.model.request.WalletRequest;
import com.sirius.koshelek.model.response.WalletResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class WalletConverterTest {

    @Autowired
    private WalletConverter walletConverter;

    @Test
    void toWalletResponse() {
        Wallet wallet = WalletBuilder.aWallet().build();
        WalletResponse walletResponse = WalletResponseBuilder.aWalletResponse().build();

        assertThat(walletResponse).isEqualTo(walletConverter.toWalletResponse(wallet));
    }

    @Test
    void toWalletResponseWithoutLimit() {
        Wallet wallet = WalletBuilder.aWallet().withLimit(null).build();
        WalletResponse walletResponse = WalletResponseBuilder.aWalletResponse().withLimit(null).build();

        assertThat(walletResponse).isEqualTo(walletConverter.toWalletResponse(wallet));
    }

    @Test
    void toWallet() {
        Wallet emptyWallet = WalletBuilder
                .aWallet().withBalance(null).withIncome(null).withOutcome(null).withId(null).withUserId(null).build();
        WalletRequest walletRequest = WalletRequestBuilder.aWalletRequest().build();

        Wallet result = walletConverter.toWallet(walletRequest);
        result.setCurrency(CurrencyBuilder.aCurrency().build());

        assertThat(emptyWallet).isEqualTo(result);
    }
}
