package com.sirius.koshelek.converter;

import com.sirius.koshelek.builder.entity.CategoryBuilder;
import com.sirius.koshelek.builder.entity.CurrencyBuilder;
import com.sirius.koshelek.builder.entity.OperationBuilder;
import com.sirius.koshelek.builder.request.OperationRequestBuilder;
import com.sirius.koshelek.builder.response.OperationResponseBuilder;
import com.sirius.koshelek.db.entity.Operation;
import com.sirius.koshelek.model.request.OperationRequest;
import com.sirius.koshelek.model.response.OperationResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class OperationConverterTest {

    @Autowired
    private OperationConverter operationConverter;

    @Test
    void toOperationResponse() {
        Operation operation = OperationBuilder.aOperation().build();
        OperationResponse operationResponse = OperationResponseBuilder.aOperationResponse().build();

        assertThat(operationResponse).isEqualTo(operationConverter.toOperationResponse(operation));
    }

    @Test
    void toOperation() {
        Operation operation = OperationBuilder.aOperation().withId(null).withWalletId(null).withCurrency(null).build();
        OperationRequest operationRequest = OperationRequestBuilder.aOperationRequest().build();

        Operation result = operationConverter.toOperation(operationRequest, CategoryBuilder.aCategory().build(), CurrencyBuilder.aCurrency().build());
        result.setCategory(CategoryBuilder.aCategory().build());
        result.setCurrency(CurrencyBuilder.aCurrency().build());

        assertThat(operation).isEqualTo(result);
    }
}
