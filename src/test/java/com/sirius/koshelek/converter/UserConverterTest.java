package com.sirius.koshelek.converter;

import com.sirius.koshelek.builder.entity.UserBuilder;
import com.sirius.koshelek.builder.request.UserRequestBuilder;
import com.sirius.koshelek.builder.response.UserResponseBuilder;
import com.sirius.koshelek.db.entity.User;
import com.sirius.koshelek.model.request.UserRequest;
import com.sirius.koshelek.model.response.UserResponse;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class UserConverterTest {
    private final UserConverter userConverter = Mappers.getMapper(UserConverter.class);

    @Test
    void toUserResponse() {
        User user = UserBuilder.aUser().build();
        UserResponse userResponse = UserResponseBuilder.aUserResponse().build();

        assertThat(userResponse).isEqualTo(userConverter.toUserResponse(user));
    }

    @Test
    void toUser() {
        User user = UserBuilder.aUser().withId(null).build();
        UserRequest userRequest = UserRequestBuilder.aUserRequest().build();

        assertThat(user).isEqualTo(userConverter.toUser(userRequest));
    }
}
