package com.sirius.koshelek.converter;

import com.sirius.koshelek.builder.entity.CategoryBuilder;
import com.sirius.koshelek.builder.entity.CurrencyBuilder;
import com.sirius.koshelek.builder.entity.OperationBuilder;
import com.sirius.koshelek.builder.request.CategoryRequestBuilder;
import com.sirius.koshelek.builder.response.CategoryResponseBuilder;
import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.model.request.CategoryRequest;
import com.sirius.koshelek.model.response.CategoryResponse;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class CategoryConverterTest {

    CategoryConverter categoryConverter = Mappers.getMapper(CategoryConverter.class);

    @Test
    void toCategoryResponse() {
        Category category = CategoryBuilder.aCategory().build();
        CategoryResponse categoryResponse = CategoryResponseBuilder.aCategoryResponse().build();

        assertThat(categoryResponse).isEqualTo(categoryConverter.toCategoryResponse(category));
    }

    @Test
    void toCategory() {
        Category category = CategoryBuilder.aCategory().withId(null).build();
        CategoryRequest categoryRequest = CategoryRequestBuilder.aCategoryRequest().build();

        assertThat(category).isEqualTo(categoryConverter.toCategory(categoryRequest));
    }
}
