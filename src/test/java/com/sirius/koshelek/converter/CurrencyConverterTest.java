package com.sirius.koshelek.converter;

import com.sirius.koshelek.builder.entity.CurrencyBuilder;
import com.sirius.koshelek.builder.response.CurrencyResponseBuilder;
import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.model.response.CurrencyResponse;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.assertj.core.api.Assertions.assertThat;

public class CurrencyConverterTest {

    private final CurrencyConverter currencyConverter = Mappers.getMapper(CurrencyConverter.class);

    @Test
    void toCurrencyResponse() {
        Currency currency = CurrencyBuilder.aCurrency().build();
        CurrencyResponse currencyResponse = CurrencyResponseBuilder.aCurrencyResponse().build();

        assertThat(currencyResponse).isEqualTo(currencyConverter.toCurrencyResponse(currency));
    }
}
