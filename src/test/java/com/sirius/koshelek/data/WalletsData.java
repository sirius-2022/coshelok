package com.sirius.koshelek.data;

import com.sirius.koshelek.builder.entity.WalletBuilder;
import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.db.entity.Wallet;

import java.math.BigDecimal;

public class WalletsData {

    public static Wallet createWallet(Currency currency, long balance, long income, long outcome) {
        return WalletBuilder.aWallet()
                .withCurrency(currency)
                .withLimit(money(0))
                .withBalance(money(balance))
                .withIncome(money(income))
                .withOutcome(money(outcome))
                .withLimit(money(0)).build();
    }

    public static BigDecimal money(long value) {
        return BigDecimal.valueOf(value);
    }
}
