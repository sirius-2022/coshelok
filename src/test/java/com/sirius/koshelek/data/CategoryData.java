package com.sirius.koshelek.data;

import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.model.OperationType;

import java.util.List;
import java.util.stream.Stream;

import static com.sirius.koshelek.model.OperationType.INCOME;
import static com.sirius.koshelek.model.OperationType.OUTCOME;

public class CategoryData {
    public static Category SALARY = createCategory(1L, "Salary", INCOME, 1L, 1L);
    public static Category GIFT = createCategory(2L, "Gift", INCOME, 2L, 2L);
    public static Category SIDE_JOB = createCategory(3L, "Side job", INCOME, 3L, 3L);
    public static Category CAPITALIZATION = createCategory(4L, "Capitalization", INCOME, 4L, 4L);

    public static Category FOOD = createCategory(5L, "Food", OUTCOME, 5L, 5L);
    public static Category SPORT = createCategory(6L, "Sport", OUTCOME, 6L, 6L);
    public static Category MEDICINE = createCategory(7L, "Medicine", OUTCOME, 7L, 7L);
    public static Category TRANSPORT = createCategory(8L, "Transport", OUTCOME, 8L, 8L);
    public static Category VACATION = createCategory(9L, "Capitalization", OUTCOME, 9L, 9L);

    public static List<Category> INCOME_CATEGORIES = List.of(
            SALARY, GIFT, SIDE_JOB, CAPITALIZATION
    );

    public static List<Category> OUTCOME_CATEGORIES = List.of(
            FOOD, SPORT, MEDICINE, TRANSPORT, VACATION
    );

    public static List<Category> CATEGORIES = Stream.concat(INCOME_CATEGORIES.stream(), OUTCOME_CATEGORIES.stream())
            .toList();


    public static Category createCategory(Long id, String name, OperationType type, Long icon, Long color) {
        Category category = new Category();
        category.setId(id);
        category.setName(name);
        category.setType(type);
        category.setIcon(icon);
        category.setColor(color);

        return category;
    }

}
