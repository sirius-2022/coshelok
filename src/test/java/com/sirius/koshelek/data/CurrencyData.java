package com.sirius.koshelek.data;

import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.model.response.CurrencyResponse;

public class CurrencyData {

    public static Currency RUB = createCurrency(1L, "Russian ruble", "RUB");
    public static Currency USD = createCurrency(2L, "United States dollar", "USD");
    public static Currency EUR = createCurrency(3L, "Euro", "EUR");

    public static CurrencyResponse RUB_RESPONSE = createCurrencyResponse(1L, "Russian ruble", "RUB");
    public static CurrencyResponse USD_RESPONSE = createCurrencyResponse(2L, "United States dollar", "USD");
    public static CurrencyResponse EUR_RESPONSE = createCurrencyResponse(3L, "Euro", "EUR");

    private static Currency createCurrency(Long id, String name, String code) {
        Currency currency = new Currency();
        currency.setId(id);
        currency.setName(name);
        currency.setCode(code);

        return currency;
    }

    private static CurrencyResponse createCurrencyResponse(Long id, String name, String code) {
        return new CurrencyResponse(id, name, code);
    }
}
