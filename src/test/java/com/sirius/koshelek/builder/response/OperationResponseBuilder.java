package com.sirius.koshelek.builder.response;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.OperationType;
import com.sirius.koshelek.model.response.CategoryResponse;
import com.sirius.koshelek.model.response.CurrencyResponse;
import com.sirius.koshelek.model.response.OperationResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aOperationResponse")
public class OperationResponseBuilder implements TestBuilder<OperationResponse> {
    private long id = 0L;
    private OperationType type = OperationType.INCOME;
    private CategoryResponse category;
    private CurrencyResponse currency;
    private BigDecimal value = BigDecimal.valueOf(100);
    private LocalDateTime date;

    @Override
    public OperationResponse build() {
        currency = new CurrencyResponse(0, "Russian currency", "RUB");
        category = new CategoryResponse(0L, "Salary", OperationType.INCOME, 0, 0);
        date = LocalDateTime.of(2022,8,13,16,0, 0, 123456789);
        return new OperationResponse(id, type, category, currency, value, date);
    }
}
