package com.sirius.koshelek.builder.response;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.response.UserResponse;
import lombok.*;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aUserResponse")
public class UserResponseBuilder implements TestBuilder<UserResponse> {
    private Long id = 0L;
    private String name = "Jmishenko Valeriy Albertovich";
    private String login = "Jma@gmail.com";

    @Override
    public UserResponse build() {
        return new UserResponse(id, name, login);
    }
}
