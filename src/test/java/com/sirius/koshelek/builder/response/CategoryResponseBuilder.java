package com.sirius.koshelek.builder.response;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.OperationType;
import com.sirius.koshelek.model.request.CategoryRequest;
import com.sirius.koshelek.model.response.CategoryResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aCategoryResponse")
public class CategoryResponseBuilder implements TestBuilder<CategoryResponse> {
    private String name = "Salary";
    private OperationType type = OperationType.INCOME;
    private Integer icon = 0;
    private Integer color = 0;

    @Override
    public CategoryResponse build() {
        return new CategoryResponse(0L, name, type, icon, color);
    }
}
