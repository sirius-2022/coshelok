package com.sirius.koshelek.builder.response;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.response.CurrencyResponse;
import com.sirius.koshelek.model.response.WalletResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

import java.math.BigDecimal;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aWalletResponse")
public class WalletResponseBuilder implements TestBuilder<WalletResponse> {

    private long id = 0L;
    private String name = "Wallet1";
    private BigDecimal limit = BigDecimal.valueOf(3000);
    private BigDecimal balance = BigDecimal.valueOf(1500);
    private BigDecimal income = BigDecimal.valueOf(1600);
    private BigDecimal outcome = BigDecimal.valueOf(100);
    private CurrencyResponse currency = new CurrencyResponse(0L, "Russian currency", "RUB");
    private Boolean overLimit = false;

    @Override
    public WalletResponse build() {
        return new WalletResponse(id, name, limit, balance, income, outcome, currency, overLimit);
    }
}
