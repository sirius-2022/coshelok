package com.sirius.koshelek.builder.response;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.response.CurrencyResponse;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aCurrencyResponse")
public class CurrencyResponseBuilder implements TestBuilder<CurrencyResponse> {
    private long id = 0L;
    private String name = "Russian currency";
    private String code = "RUB";

    @Override
    public CurrencyResponse build() {
        return new CurrencyResponse(id, name, code);
    }
}
