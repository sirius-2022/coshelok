package com.sirius.koshelek.builder.entity;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.db.entity.Currency;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aCurrency")
public class CurrencyBuilder implements TestBuilder<Currency> {
    private Long id = 0L;
    private String name = "Russian currency";
    private String code = "RUB";

    @Override
    public Currency build() {
        Currency currency = new Currency();
        currency.setId(id);
        currency.setCode(code);
        currency.setName(name);
        return currency;
    }
}
