package com.sirius.koshelek.builder.entity;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.model.OperationType;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aCategory")
public class CategoryBuilder implements TestBuilder<Category> {
    private Long id = 0L;
    private String name = "Salary";
    private OperationType type = OperationType.INCOME;
    private Long icon = 0L;
    private Long color = 0L;

    @Override
    public Category build() {
        Category category = new Category();
        category.setId(id);
        category.setName(name);
        category.setType(type);
        category.setIcon(icon);
        category.setColor(color);
        return category;
    }
}
