package com.sirius.koshelek.builder.entity;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.db.entity.Category;
import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.db.entity.Operation;
import com.sirius.koshelek.model.OperationType;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aOperation")
public class OperationBuilder implements TestBuilder<Operation> {
    private Long id = 0L;
    private Long walletId = 0L;
    private OperationType type = OperationType.INCOME;
    private Category category;
    private Currency currency;
    private BigDecimal value = BigDecimal.valueOf(100);
    private LocalDateTime date;

    @Override
    public Operation build() {
        date = LocalDateTime.of(2022,8,13,16,0, 0, 123456789);
        category = new Category();
        category.setId(0L);
        category.setName("Salary");
        category.setType(OperationType.INCOME);
        category.setIcon(0L);
        category.setColor(0L);

        currency = new Currency();
        currency.setId(0L);
        currency.setCode("RUB");
        currency.setName("Russian currency");

        Operation operation = new Operation();
        operation.setId(id);
        operation.setWalletId(walletId);
        operation.setType(type);
        operation.setCategory(category);
        operation.setCurrency(currency);
        operation.setValue(value);
        operation.setDate(date);
        return operation;
    }
}
