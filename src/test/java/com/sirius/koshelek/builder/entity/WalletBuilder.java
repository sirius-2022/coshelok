package com.sirius.koshelek.builder.entity;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.db.entity.Currency;
import com.sirius.koshelek.db.entity.Wallet;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

import java.math.BigDecimal;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aWallet")
public class WalletBuilder implements TestBuilder<Wallet> {

    private Long id = 0L;
    private Long userId = 0L;
    private String name = "Wallet1";
    private Currency currency;
    private BigDecimal limit = BigDecimal.valueOf(3000);
    private BigDecimal balance = BigDecimal.valueOf(1500);
    private BigDecimal income = BigDecimal.valueOf(1600);
    private BigDecimal outcome = BigDecimal.valueOf(100);
    private Boolean isDeleted = null;

    @Override
    public Wallet build() {
        if (currency == null) {
            currency = new Currency();
            currency.setId(0L);
            currency.setCode("RUB");
            currency.setName("Russian currency");
        }

        Wallet wallet = new Wallet();
        wallet.setId(id);
        wallet.setUserId(userId);
        wallet.setName(name);
        wallet.setCurrency(currency);
        wallet.setLimit(limit);
        wallet.setIncome(income);
        wallet.setOutcome(outcome);
        wallet.setIsDeleted(isDeleted);
        return wallet;
    }
}
