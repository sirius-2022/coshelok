package com.sirius.koshelek.builder.entity;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.db.entity.User;
import lombok.*;


@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aUser")
public class UserBuilder implements TestBuilder<User> {
    private Long id = 0L;
    private String name = "Jmishenko Valeriy Albertovich";
    private String login = "Jma@gmail.com";

    @Override
    public User build() {
        final var user = new User();
        user.setId(id);
        user.setName(name);
        user.setLogin(login);
        return user;
    }
}
