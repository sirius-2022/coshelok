package com.sirius.koshelek.builder;

public interface TestBuilder<T> {
    T build();
}
