package com.sirius.koshelek.builder.request;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.request.UserRequest;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aUserRequest")
public class UserRequestBuilder implements TestBuilder<UserRequest> {
    private String name = "Jmishenko Valeriy Albertovich";
    private String login = "Jma@gmail.com";

    @Override
    public UserRequest build() {
        return new UserRequest(name, login);
    }
}
