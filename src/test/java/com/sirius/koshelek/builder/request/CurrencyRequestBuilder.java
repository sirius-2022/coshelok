package com.sirius.koshelek.builder.request;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.request.CurrencyRequest;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aCurrencyRequest")
public class CurrencyRequestBuilder implements TestBuilder<CurrencyRequest> {
    private long id = 0L;
    private String name = "Russian currency";
    private String code = "RUB";

    @Override
    public CurrencyRequest build() {
        return new CurrencyRequest(id, name, code);
    }
}
