package com.sirius.koshelek.builder.request;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.request.WalletRequest;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

import java.math.BigDecimal;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aWalletRequest")
public class WalletRequestBuilder implements TestBuilder<WalletRequest> {

    private String name = "Wallet1";
    private Long currency = 0L;
    private BigDecimal limit = BigDecimal.valueOf(3000);

    @Override
    public WalletRequest build() {
        return new WalletRequest(name, currency, limit);
    }
}
