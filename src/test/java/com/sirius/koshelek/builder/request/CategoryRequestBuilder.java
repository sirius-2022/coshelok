package com.sirius.koshelek.builder.request;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.OperationType;
import com.sirius.koshelek.model.request.CategoryRequest;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aCategoryRequest")
public class CategoryRequestBuilder implements TestBuilder<CategoryRequest> {
    private String name = "Salary";
    private OperationType type = OperationType.INCOME;
    private Integer icon = 0;
    private Integer color = 0;

    @Override
    public CategoryRequest build() {
        return new CategoryRequest(name, type, icon, color);
    }
}
