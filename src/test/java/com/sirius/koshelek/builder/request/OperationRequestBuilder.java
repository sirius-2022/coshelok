package com.sirius.koshelek.builder.request;

import com.sirius.koshelek.builder.TestBuilder;
import com.sirius.koshelek.model.OperationType;
import com.sirius.koshelek.model.request.OperationRequest;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.With;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@With
@AllArgsConstructor
@NoArgsConstructor(staticName = "aOperationRequest")
public class OperationRequestBuilder implements TestBuilder<OperationRequest> {
    private OperationType type = OperationType.INCOME;
    private Long category = 0L;
    private Long currency = 0L;
    private BigDecimal value = BigDecimal.valueOf(100);
    private LocalDateTime date = LocalDateTime.of(2022,8,13,16,0, 0, 123456789);

    @Override
    public OperationRequest build() {
        return new OperationRequest(type, category, currency, value, date);
    }
}
